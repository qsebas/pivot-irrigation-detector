import cv2
import cv2.cv as cv
import numpy as np
import datetime
import os
import sys
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('files', nargs='+', help='Files to be processed')
parser.add_argument('-dp', default= 1, help='Inverse ratio of the accumulator resolution to the image resolution. For example, if dp=1 , the accumulator has the same resolution as the input image. If dp=2 , the accumulator has half as big width and height.')
parser.add_argument('--minDist', '-md', default= 15, type=int, help='Minimum distance between the centers of the detected circles. If the parameter is too small, multiple neighbor')
parser.add_argument('--param1', '-p1', default=200, type=int, help='The higher threshold of the two passed to the Canny() edge detector (the lower one is twice smaller).')
parser.add_argument('--param2', '-p2', default=30, type=int, help='Accumulator threshold for the circle centers at the detection stage. The smaller it is, the more false circles may be detected. Circles, corresponding to the larger accumulator values, will be returned first.')
parser.add_argument('--minRadius', '-r', default=10, type=int, help='Minimum circle radius')
parser.add_argument('--maxRadius', '-R', default=50, type=int, help='Maximum circle radius')
parser.add_argument('--debug', default=False, action='store_true', help='writes temporal images')
args = parser.parse_args(sys.argv[1:])
if args.debug:
	print "OpenCV version :  {0}".format(cv2.__version__)

_dp = args.dp
_minDist = args.minDist
_param1 = args.param1
_param2 = args.param2
_minRadius = args.minRadius
_maxRadius = args.maxRadius
files = args.files
print files
print files
for inputfile in files:
    print inputfile
    img = cv2.imread(inputfile, cv2.IMREAD_GRAYSCALE)
    img = cv2.medianBlur(img,5)
    edges = cv2.Canny(img,_param1/2,_param1)
    #img = cv2.bilateralFilter(img,9,75,75)
    cimg = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)

    #circles = cv2.HoughCircles(img ,cv.CV_HOUGH_GRADIENT, 1, 20, param1=70,param2=40, minRadius=25, maxRadius=85)  
    circles = cv2.HoughCircles(img ,cv.CV_HOUGH_GRADIENT, dp=_dp, minDist=_minDist, param1=_param1, param2=_param2, minRadius=_minRadius, maxRadius=_maxRadius) 
						#1, 20, param1=50,param2=30,minRadius=0,maxRadius=0)
    if circles is not None:
        circles = np.uint16(np.around(circles))
        for i in circles[0,:]:
            # draw the outer circle
            cv2.circle(cimg,(i[0],i[1]),i[2],(0,255,0),2)
            # draw the center of the circle
            cv2.circle(cimg,(i[0],i[1]),2,(0,0,255),3)		
    head, tail = os.path.split(inputfile)
    tail = os.path.splitext(tail)[0]
    cv2.imwrite(os.path.join('out', "{}.{}.dp-{}.md-{}.p1-{}.p2-{}.r-{}.R-{}.jpg".format( tail ,datetime.datetime.now().strftime("%Y%m%d-%H%M%S"), _dp, _minDist, _param1, _param2, _minRadius, _maxRadius )),cimg)
    if args.debug:
        cv2.imwrite(os.path.join('out', "cannny.{}.jpg").format(tail), edges)
        cv2.imwrite(os.path.join('out', "blur.{}.jpg").format(tail), img)
